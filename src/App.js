import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import Nav from "./components/Nav/Nav";
import Posts from "./containers/Posts/Posts";
import AddPost from "./components/AddPost/AddPost";
import FullPost from "./components/FullPost/FullPost";
import About from "./components/About/About";
import Contacts from "./components/Contacts/Contacts";

class App extends Component {
  render() {
    const textStyle = {
      textAlign: 'center'
    };

    return (
      <Fragment>
        <Nav/>
        <Switch>
          <Route exact path="/" component={Posts} />
          <Route exact path="/posts" component={Posts} />
          <Route exact path="/posts/add" component={AddPost} />
          <Route exact path="/posts/:id" component={FullPost} />    
          <Route exact path="/posts/:id/edit" component={AddPost} />
          <Route path="/about" component={About} />
          <Route path="/contacts" component={Contacts} />
          <Route render={() => <h1 style={textStyle}>Page not found</h1>} />
        </Switch>
      </Fragment>
    );
  }
}

export default App;
