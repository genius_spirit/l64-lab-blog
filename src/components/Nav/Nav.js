import React from 'react';
import './Nav.css';
import {NavLink} from "react-router-dom";

const Nav = () => {
  return(
    <nav className='main-nav'>
      <ul>
        <li><NavLink to="/">Home</NavLink></li>
        <li><NavLink to="/posts/add">Add</NavLink></li>
        <li><NavLink to="/about">About us</NavLink></li>
        <li><NavLink to="/contacts">Contacts</NavLink></li>
      </ul>
    </nav>
  )
};

export default Nav;