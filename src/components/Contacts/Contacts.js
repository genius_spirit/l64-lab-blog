import React from 'react';
import './Contacts.css';
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => <div className='marker'>{text}</div>;

const Contacts = () => {
  return(
    <div className="map">
      <h3>Мы на карте:</h3>
      <GoogleMapReact
        bootstrapURLKeys={{key: 'AIzaSyCKNSTOkflOe89s2LF8hk2erR15tSuTt4I'}}
        center={{lat: 42.9231899, lng: 74.6316946}}
        zoom={10}
      >
        <AnyReactComponent
          lat={42.8752771}
          lng={74.603956}
          text={'Company name'}
        />
      </GoogleMapReact>
    </div>
  )
};

export default Contacts;