import React, { Component } from 'react';
import axios from 'axios';
import './Posts.css';
import {Link} from "react-router-dom";

class Posts extends Component {

  state = {
    posts: []
  };

  getPost = () => {
    axios.get('/post.json').then(response => {
      response.data ? this.setState({posts: response.data}) : this.setState({posts: []});
    })
  };


  componentDidMount() {
    this.getPost();
  }

  render() {
    return(
      <div className='container'>
        {Object.keys(this.state.posts).map(itemId => {
          return (
            <div key={itemId} className="post-item">
              <div className="post-time">Created on: {this.state.posts[itemId].time}</div>
              <p>{this.state.posts[itemId].title}</p>
              <Link to={"/posts/" + itemId} className="post-btn">Read more >>></Link>
            </div>
          )
        })}
      </div>
    )
  }
}

export default Posts;